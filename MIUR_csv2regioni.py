#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##en
#    MIUR_csv2regioni, converts some CSV files containing Open Data
#    from MIUR into a collection of CSV files with essential
#    information, region by region.
#    Copyright (C) 2017 Marco BODRATO
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##it
#    MIUR_csv2regioni, converte alcuni file CSV di Open Data del MIUR
#    in un ventaglio di file CSV con informazioni essenziali, regione
#    per regione.
#    Scritto nel 2016-2017 da Marco BODRATO, che ne detiene i diritti d'autore
#
#    Questo programma è software libero: può essere ridistribuito o modificato
#    nei termini della licenza "GNU Affero General Public License" come
#    pubblicata dalla Free Software Foundation; o la versione 3 della
#    licenza, o (a vostra scelta) qualunque versione successiva.
#
#    Questo programma è distribuito con l'auspicio che sia utile,
#    ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
#    VENDIBILITÀ o ADEGUATEZZA AD UN PARTICOLARE SCOPO. Si veda la
#    "GNU Affero General Public License" per ulteriori dettagli.
#
#    Dovreste aver ricevuto una copia della "GNU Affero General Public License"
#    con a questo programma. Vedere altrimenti <http://www.gnu.org/licenses/>.
#

import csv
from sys import exit
from operator import itemgetter

def normalizzaIndirizzo (indirizzo):
    """Parziale normalizzazione degli indirizzi: impone uno spazio dopo la
    virgola, elimina spazi doppi o marginali e rende tutto maiuscolo.
    Inoltre normalizza alcune abbreviazioni soprattutto per la tipologia
    (via, corso, strada...) ed alcune indicazioni di numero civico."""
    indirizzo = indirizzo.upper().replace (',', ' , ').replace('"','').replace('À ',"A' ").replace (' -', ' - ').replace ('- ', ' - ')
    while '  ' in indirizzo:
        indirizzo = indirizzo.replace ('  ', ' ')
    indirizzo = indirizzo.replace(' ,',',').replace(' .','.')
    while indirizzo and indirizzo[0] in ', .-':
        indirizzo = indirizzo[1:]
    if indirizzo:
        while indirizzo[-1] in ', ':
            indirizzo = indirizzo[:-1]
        if indirizzo[:2] in ['V.', 'V/']:
            if indirizzo[2] == ' ' or indirizzo[2:5] == 'LE ':
                indirizzo = 'VIA' + indirizzo[2:]
            elif indirizzo[2:5] != 'LO ':
                indirizzo = 'VIA ' + indirizzo[2:]
        elif indirizzo[:4] in ['VIA.', 'VAI ', 'V1A ']:
            indirizzo = 'VIA ' + indirizzo[4:]
        elif indirizzo[:4] == 'VLE ':
            indirizzo = 'VIA' + indirizzo[1:]
        elif indirizzo[:4] == 'C.O ':
            indirizzo = 'CORS' + indirizzo[2:]
        elif indirizzo[:5] in ['C.SO ', 'C/SO ']:
            indirizzo = 'COR' + indirizzo[2:]
        elif indirizzo[:6] == 'C. SO ':
            indirizzo = 'COR' + indirizzo[3:]
        elif indirizzo[:5] == 'B.DO ':
            indirizzo = 'BALUAR' + indirizzo[2:]
        elif indirizzo[:5] == 'B.GO ':
            indirizzo = 'BOR' + indirizzo[2:]
        elif indirizzo[:5] == 'B.TA ':
            indirizzo = 'BORGA' + indirizzo[2:]
        elif indirizzo[:5] == 'B.DO ':
            indirizzo = 'BALUAR' + indirizzo[2:]
        elif indirizzo[:8] in ['CONT.DA ', 'CONTADA']:
            indirizzo = 'CONTRA' + indirizzo[5:]
        elif indirizzo[:8] in ['CIRC.NE ', 'CIRC/NE ']:
            indirizzo = 'CIRCONVALLAZIO' + indirizzo[5:]
        elif indirizzo[:9] == 'CIRCONV. ':
            indirizzo = 'CIRCONVALLAZIONE' + indirizzo[8:]
        elif indirizzo[:11] == 'CIRCONV.NE ':
            indirizzo = 'CIRCONVALLAZIO' + indirizzo[8:]
        elif indirizzo[:5] == 'L.GO ':
            indirizzo = 'LAR' + indirizzo[2:]
        elif indirizzo[:6] == 'L.GO. ':
            indirizzo = 'LARGO' + indirizzo[5:]
        elif indirizzo[:6] in ['FRAZ. ', 'FRAZ: ']:
            indirizzo = 'FRAZIONE' + indirizzo[5:]
        elif indirizzo[:5] in ['FRAZ.', 'FRAZ '] or indirizzo[:4] in ['FRA.', 'FRZ.']:
            indirizzo = 'FRAZIONE ' + indirizzo[5:]
        elif indirizzo[:6] == 'ST.DA ':
            indirizzo = 'STRA' + indirizzo[3:]
        elif indirizzo[:7] == 'STRAD. ':
            indirizzo = 'STRADA' + indirizzo[6:]
        elif indirizzo[:5] == 'STR. ':
            indirizzo = 'STRADA' + indirizzo[4:]
        elif indirizzo[:7] == 'REG.NE ':
            indirizzo = 'REGIONE' + indirizzo[6:]
        elif indirizzo[:5] == 'REG. ':
            indirizzo = 'REGIONE' + indirizzo[4:]
        elif indirizzo[:5] == 'Q.RE ':
            indirizzo = 'QUARTIE' + indirizzo[2:]
        elif indirizzo[:5] == 'C.DA ':
            indirizzo = 'CONTRA' + indirizzo[2:]
        elif indirizzo[:3] in ['PIZ', 'PZZ']:
            indirizzo = 'PIA' + indirizzo[2:]
        elif indirizzo[:5] in ['PIAZA', 'PIAZ.']:
            indirizzo = 'PIAZZA' + indirizzo[5:]
        elif indirizzo[:7] == 'PUIAZZA':
            indirizzo = 'PIAZZA' + indirizzo[7:]
        elif indirizzo[:8] == 'PIAZZ.LE':
            indirizzo = 'PIAZZA' + indirizzo[6:]
        elif indirizzo[:4] in ['PZ. ', 'PZA ']:
            indirizzo = 'PIAZZA' + indirizzo[3:]
        elif indirizzo[:3] == 'LOC':
            if indirizzo[3:5] == '. ':
                indirizzo = "LOCALITA'" + indirizzo[4:]
            elif indirizzo[3:7] == 'AL. ':
                indirizzo = "LOCALITA'" + indirizzo[6:]
            elif indirizzo[3:9] == 'ALITA ':
                indirizzo = "LOCALITA'" + indirizzo[8:]
            elif indirizzo[3:10] == "LAITA' ":
                indirizzo = 'LOCAL' + indirizzo[5:]
            elif indirizzo[3:11] == "ALIATA' ":
                indirizzo = 'LOCALI' + indirizzo[7:]
            elif indirizzo[3] in '. ':
                indirizzo = "LOCALITA' " + indirizzo[4:]
            elif indirizzo[3:10] != "ALITA' ":
                indirizzo = "LOCALITA' " + indirizzo[9:]
        elif indirizzo[:2] in ['P.', 'P/', 'P-']:
            if indirizzo[2:5] in ['AZA' , 'ZZA']:
                indirizzo = 'PIAZ' + indirizzo[3:]
            elif indirizzo[2:5] == 'ZA ':
                indirizzo = 'PIAZ' + indirizzo[2:]
            elif indirizzo[2:5] == 'TA ':
                indirizzo = 'PIAZZET' + indirizzo[2:]
            elif indirizzo[2:6] == 'TTA ':
                indirizzo = 'PIAZZE' + indirizzo[2:]
            elif indirizzo[2:7] == ' ZZA ':
                indirizzo = 'PIA' + indirizzo[3:]
            elif indirizzo[2] == ' ' or indirizzo[2:5] == 'LE ':
                indirizzo = 'PIAZZA' + indirizzo[2:]
        indice = indirizzo.rfind (' N')
        if indice > 0 and len (indirizzo) > indice + 3:
            ind2 = -1
            if indirizzo[indice + 2] in '0123456789':
                ind2 = indice + 2
            elif indirizzo[indice + 3] in '0123456789':
                ind2 = indice + 3
            elif indirizzo[indice + 3] == ' ' and indirizzo[indice + 4] in '0123456789':
                ind2 = indice + 4
            if ind2 > indice:
                if indirizzo[indice - 1] == ',':
                    indirizzo = indirizzo [:indice + 1] + indirizzo[ind2:]
                else:
                    indirizzo = indirizzo [:indice] + ', ' + indirizzo[ind2:]
        indice = indirizzo.rfind (' ')
        if indirizzo [indice+1] == 'S' and indirizzo[indice+2:] in ['N', 'NC', 'NC.', 'N.', '.N', '.N.', '.N.C', 'N.C', '.N.C.']:
            if indirizzo[indice - 1] == ',':
                indirizzo = indirizzo [:indice + 1] + 'SNC'
            else:
                indirizzo = indirizzo [:indice] + ', SNC'
        indice = indirizzo.rfind (' ')
        if 'KM' in indirizzo:
            indirizzo = indirizzo.replace ('KM. ','KM ').replace ('KM.','KM ')
            if indirizzo.rfind(', ') > indirizzo.rfind('KM'):
                if indirizzo[indirizzo.rfind(', ')+2] in '0123456789':
                    indirizzo = indirizzo[::-1].replace(' ,',',',1)[::-1]
        elif indirizzo [indice+1] in '123456789' and indirizzo [indice-1] != ',':
            indirizzo = indirizzo [:indice] + ',' + indirizzo [indice:]
    return indirizzo.replace(' -,',',').replace ('F.LLI','FRATELLI')

def leggiInfoRegioni (Regioni = {}, nomeFileDati = 'Regioni.csv', separatoreDati = '|'):
    try:
        fileCsv = open (nomeFileDati)
        csvRegioni = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvRegioni:
            Regioni[riga['DenominazioneMIUR']] = [riga['NomeFile'], riga['CR'], set()]
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()
    return Regioni

def leggiCSVScuolaInChiaro (ScuoleWGS84, nomeFileDati):
    separatoreDati = ';'
    try:
        fileCsv = open (nomeFileDati)
        csvScuole = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvScuole:
            if len (riga['LATITUDINE']) + len (riga['LONGITUDINE']) > 2:
                if riga['PLESSO/SCUOLA'] in ScuoleWGS84:
                    print ('Codice duplicato:', riga['PLESSO/SCUOLA'])
                else:
                    lat = riga['LATITUDINE'].replace(",",".")
                    lon = riga['LONGITUDINE'].replace(",",".")
                    ind = normalizzaIndirizzo (riga['INDIRIZZO'])
                    ScuoleWGS84[riga['PLESSO/SCUOLA']] = (riga['COMUNE'], ind, lat, lon, riga['REGIONE'].upper())
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()

def leggiCSVopenDataMIUR (Scuole, Regioni, ScuoleWGS84, Note, nomeFileDati, nonStat = '', separatoreDati = ','):
    istRef = nonStat
    try:
        fileCsv = open (nomeFileDati)
        csvScuole = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvScuole:
            scuola = riga[' CODICESCUOLA']
            Regioni[riga[' REGIONE']][2].add (scuola)
            capScuola = riga[' CAPSCUOLA']
            if len(capScuola) != 5:
                if len(capScuola) < 5 and len(capScuola) > 1 :
                    capScuola = ('00000' + capScuola) [-5:]
                else:
                    capScuola = 'NDISP'
            chk = ''
            latScuola = ''
            lonScuola = ''
            indScuola = normalizzaIndirizzo (riga[' INDIRIZZOSCUOLA'])
            if nonStat == '':
                istRef = riga[' CODICEISTITUTORIFERIMENTO']
            if scuola in Scuole:
                print ('Codice duplicato:', scuola)
            elif scuola in ScuoleWGS84:
                scuolaWGS84 = ScuoleWGS84.pop(scuola)
                if scuolaWGS84[0] != riga[' DESCRIZIONECOMUNE']:
                    chk = '?C'
                elif scuolaWGS84[1] != indScuola[:len(scuolaWGS84[1])]:
                    chk = '?i'
                latScuola = scuolaWGS84[2]
                lonScuola = scuolaWGS84[3]
            if scuola in Note:
                chk = chk + Note.pop(scuola)
            provScuola = riga[' PROVINCIA']
            if 'BARLETTA' in provScuola:
                provScuola = provScuola.replace ('ADRIA', 'ANDRIA')
            elif 'MEDIO' in provScuola:
                provScuola = provScuola.replace ('CAMPITANO', 'CAMPIDANO')
            Scuole[scuola] = (indScuola, capScuola, riga[' DESCRIZIONECOMUNE'], provScuola, riga[' CODICECOMUNESCUOLA'], istRef, latScuola, lonScuola, chk)
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()

def leggiSegnalazioni (Scuole, nomeFileDati, separatoreDati = ','):
    fileCsv = open (nomeFileDati)
    csvScuole = csv.reader(fileCsv, delimiter = separatoreDati)
    intestazione = next(csvScuole)
    for riga in csvScuole:
        Scuole[riga[0]] = riga[1]

def scriviFileRegione (regione, datiRegione, Scuole, separatoreDati = ';'):
    intestazione = ['CR', 'CODICESCUOLA', 'INDIRIZZO', 'CAP', 'COMUNE', 'PROVINCIA', 'CODICECOMUNE', 'ISTITUTORIFERIMENTO', 'LAT', 'LON', 'CHK']
    listaScuole = [[datiRegione[1], x] + list(Scuole[x]) for x in datiRegione[2]]
    listaScuole.sort ()
    listaScuole.sort (key = itemgetter(intestazione.index('PROVINCIA'), intestazione.index('COMUNE'), intestazione.index('ISTITUTORIFERIMENTO')))
    scriviFileCSV (datiRegione[0] + '.csv', intestazione, listaScuole, separatoreDati)

def scriviFileCSV (nomeFileDati, intestazione, righe, separatoreDati = ';'):
    try:
        fileCsv = open (nomeFileDati, 'w')
        csvScuole = csv.writer(fileCsv, delimiter = separatoreDati, quoting=csv.QUOTE_MINIMAL)
        csvScuole.writerow(intestazione)
        for riga in righe:
            csvScuole.writerow(riga)
    except:
        print ('Errore durante la scrittura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()

def scriviFileResidui (Scuole, nomeFileDati = 'coordinateNonUsate.csv', separatoreDati = ';'):
    intestazione = ['REGIONE', 'COMUNE', 'INDIRIZZO', 'LAT', 'LON']
    listaScuole = [[Scuole[x][4]] + list (Scuole[x][:4]) for x in Scuole if ',' in Scuole[x][1] and Scuole[x][1][-4:] != " SNC"]
    listaScuole.sort ()
    scriviFileCSV (nomeFileDati, intestazione, listaScuole, separatoreDati)

Regioni = leggiInfoRegioni ()

ScuoleWGS84 = {}
leggiCSVScuolaInChiaro (ScuoleWGS84, '7-Anagrafe_Scuole_Statali_201516.csv')
leggiCSVScuolaInChiaro (ScuoleWGS84, '8-Anagrafe_Scuole_Paritarie_201516.csv')

print ('Scuole (con coordinate) da "Scuola in chiaro": ', len (ScuoleWGS84))

Note = {}
leggiSegnalazioni (Note, 'DiscrepanzeJoinISTAT.csv')

print ('Scuole con note: ', len (Note))

Scuole = {}
leggiCSVopenDataMIUR (Scuole, Regioni, ScuoleWGS84, Note, 'AS1718SCUANAGRAFESTAT20170731.csv')
leggiCSVopenDataMIUR (Scuole, Regioni, ScuoleWGS84, Note, 'AS1718SCUANAGRAFEPAR20170731.csv', '_NON_STAT_')
leggiCSVopenDataMIUR (Scuole, Regioni, ScuoleWGS84, Note, 'AS1718SCUANAAUTSTAT20170731.csv')
leggiCSVopenDataMIUR (Scuole, Regioni, ScuoleWGS84, Note, 'AS1718SCUANAAUTPAR20170731.csv', '_NON_STAT_')

print ('Scuole da "OpenData MIUR": ', len (Scuole))

for regione in Regioni:
    scriviFileRegione (regione, Regioni[regione], Scuole)

print ('Coordinate rimaste da "Scuola in chiaro": ', len (ScuoleWGS84))

scriviFileResidui (ScuoleWGS84)
