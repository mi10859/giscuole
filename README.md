# Dati geografici scuole #

Informazioni collezionate da diverse fonti di dati aperti.

I dati presenti in questo archivio sono relativi all'anno scolastico 2017/2018.
Quelli relativi all'anno precedente sono archiviati nella [corrispondente sezione](2016-17/).

## Operazioni effettuate ##

È disponibile un [resoconto abbastanza dettagliato delle operazioni effettuate](Operazioni.md). 

## Organizzazione dati ##

I dati sono suddivisi per regione, secondo il seguente schema (viene usato, dai programmi, [un apposito file](Regioni.csv)):

CR|DenominazioneISTAT|DenominazioneMIUR|NomeFile
--|------------------|-----------------|--------
01|Piemonte|PIEMONTE|[01-Piemonte.csv](01-Piemonte.csv)
02|Valle d'Aosta/Vallée d'Aoste|VALLE D' AOSTA|[02-ValleDAosta.csv](02-ValleDAosta.csv)
03|Lombardia|LOMBARDIA|[03-Lombardia.csv](03-Lombardia.csv)
04|Trentino-Alto Adige/Südtirol|TRENTINO-ALTO ADIGE|[04-TrentinoAltoAdige.csv](04-TrentinoAltoAdige.csv)
05|Veneto|VENETO|[05-Veneto.csv](05-Veneto.csv)
06|Friuli-Venezia Giulia|FRIULI-VENEZIA G.|[06-FriuliVeneziaGiulia.csv](06-FriuliVeneziaGiulia.csv)
07|Liguria|LIGURIA|[07-Liguria.csv](07-Liguria.csv)
08|Emilia-Romagna|EMILIA ROMAGNA|[08-EmiliaRomagna.csv](08-EmiliaRomagna.csv)
09|Toscana|TOSCANA|[09-Toscana.csv](09-Toscana.csv)
10|Umbria|UMBRIA|[10-Umbria.csv](10-Umbria.csv)
11|Marche|MARCHE|[11-Marche.csv](11-Marche.csv)
12|Lazio|LAZIO|[12-Lazio.csv](12-Lazio.csv)
13|Abruzzo|ABRUZZO|[13-Abruzzo.csv](13-Abruzzo.csv)
14|Molise|MOLISE|[14-Molise.csv](14-Molise.csv)
15|Campania|CAMPANIA|[15-Campania.csv](15-Campania.csv)
16|Puglia|PUGLIA|[16-Puglia.csv](16-Puglia.csv)
17|Basilicata|BASILICATA|[17-Basilicata.csv](17-Basilicata.csv)
18|Calabria|CALABRIA|[18-Calabria.csv](18-Calabria.csv)
19|Sicilia|SICILIA|[19-Sicilia.csv](19-Sicilia.csv)
20|Sardegna|SARDEGNA|[20-Sardegna.csv](20-Sardegna.csv)

Il dati in questi file sono strutturati secondo le seguenti colonne:

Intestazione colonna|descrizione contenuto
--------------------|---------------------
CR|Codice Regione, come da tabella
CODICESCUOLA|codice meccanografico della scuola/plesso
INDIRIZZO|indirizzo della scuola/plesso
CAP|Codice di Avviamento Postale
COMUNE|comune nel quale la scuola/plesso si trova
PROVINCIA|provincia che comprende il comune
CODICECOMUNE|codice catastale del comune
ISTITUTORIFERIMENTO|istituto di riferimento, o `_NON_STAT_` per le scuole non statali
LAT|latitudine
LON|longitudine
CHK|controlli

Nella colonna `CHK`, attualmente il valore `?i` significa che l'indirizzo non corrisponde con quello presente su *Scuola In Chiaro* contenente le scuole dell'anno scolastico 2015/16; il valore `?C` significa che non corrisponde il comune; in aggiunta i quattro valori `E0N`,`E1R`,`E2P`,`E3C`, per indicare la posizione rispettivamente fuori dalla nazione, Regione, Provincia o Comune attesi. Dopo il separatore `:` (due punti) il comune relativo alla posizione, secondo i confini ISTAT.

## Fonti ##

Per l'elenco delle scuole: l'area [Scuole](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/?area=Scuole) del [Portale Unico dei Dati della Scuola](http://dati.istruzione.it/opendata/), dati con licenza [IODL-v2.0](http://www.dati.gov.it/iodl/2.0/).

Come base per la geo-localizzazione: l'(ex-)area Open Data del portale [Scuola in Chiaro](http://cercalatuascuola.istruzione.it/cercalatuascuola/), dati senza alcuna licenza esplicitata, verranno trattati come dati con una generica licenza *con attribuzione*.

Da ISTAT: [codici statistici](http://www.istat.it/it/archivio/6789) e [confini](http://www.istat.it/it/archivio/124086) delle unità amministrative, e [basi territoriali](http://www.istat.it/it/archivio/104317), licenza da verificare.

## Possibili utilizzazioni ##

Qualunque. Già sperimentata la generazione di [mappe interattive](http://umap.openstreetmap.fr/it/map/piemonte-istituzioni-scolastiche-statali-as-2017-1_128612) o l'[automatizzazione del calcolo delle distanze tra ambiti territoriali](https://bitbucket.org/mi10859/prossimitambiti).

## Licenze d'uso ##

La licenza [AGPL](LICENCE.md) per il codice. Ancora non selezionata per i dati.