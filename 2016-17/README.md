# Dati geografici scuole, anno scolastico 2016/17 #

Informazioni collezionate da diverse fonti di dati aperti.

## Organizzazione dati ##

I dati sono suddivisi per regione, secondo il seguente schema (viene usato, dai programmi, [un apposito file](Regioni.csv)):

CR|DenominazioneISTAT|DenominazioneMIUR|NomeFile
--|------------------|-----------------|--------
01|Piemonte|PIEMONTE|[01-Piemonte.csv](01-Piemonte.csv)
02|Valle d'Aosta/Vallée d'Aoste|VALLE D' AOSTA|[02-ValleDAosta.csv](02-ValleDAosta.csv)
03|Lombardia|LOMBARDIA|[03-Lombardia.csv](03-Lombardia.csv)
04|Trentino-Alto Adige/Südtirol|TRENTINO-ALTO ADIGE|[04-TrentinoAltoAdige.csv](04-TrentinoAltoAdige.csv)
05|Veneto|VENETO|[05-Veneto.csv](05-Veneto.csv)
06|Friuli-Venezia Giulia|FRIULI-VENEZIA G.|[06-FriuliVeneziaGiulia.csv](06-FriuliVeneziaGiulia.csv)
07|Liguria|LIGURIA|[07-Liguria.csv](07-Liguria.csv)
08|Emilia-Romagna|EMILIA ROMAGNA|[08-EmiliaRomagna.csv](08-EmiliaRomagna.csv)
09|Toscana|TOSCANA|[09-Toscana.csv](09-Toscana.csv)
10|Umbria|UMBRIA|[10-Umbria.csv](10-Umbria.csv)
11|Marche|MARCHE|[11-Marche.csv](11-Marche.csv)
12|Lazio|LAZIO|[12-Lazio.csv](12-Lazio.csv)
13|Abruzzo|ABRUZZO|[13-Abruzzo.csv](13-Abruzzo.csv)
14|Molise|MOLISE|[14-Molise.csv](14-Molise.csv)
15|Campania|CAMPANIA|[15-Campania.csv](15-Campania.csv)
16|Puglia|PUGLIA|[16-Puglia.csv](16-Puglia.csv)
17|Basilicata|BASILICATA|[17-Basilicata.csv](17-Basilicata.csv)
18|Calabria|CALABRIA|[18-Calabria.csv](18-Calabria.csv)
19|Sicilia|SICILIA|[19-Sicilia.csv](19-Sicilia.csv)
20|Sardegna|SARDEGNA|[20-Sardegna.csv](20-Sardegna.csv)

Il dati in questi file sono strutturati secondo le seguenti colonne:

Intestazione colonna|descrizione contenuto
--------------------|---------------------
CR|Codice Regione, come da tabella
CODICESCUOLA|codice meccanografico della scuola/plesso
INDIRIZZO|indirizzo della scuola/plesso
CAP|Codice di Avviamento Postale
COMUNE|comune nel quale la scuola/plesso si trova
PROVINCIA|provincia che comprende il comune
CODICECOMUNE|codice catastale del comune
ISTITUTORIFERIMENTO|istituto di riferimento, o `_NON_STAT_` per le scuole non statali
LAT|latitudine
LON|longitudine
CHK|controlli

## Ulteriori informazioni ##

Per informazioni più approfondite o dati per differenti anni scolastici, consultare la [pagina principale del progetto](../).
