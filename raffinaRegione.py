#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##en
#    raffinaRegione, improve the file with data about schools of a
#    single region, by using the same coordinates when the address is
#    the same, and the coordinates of the town whenever better
#    informations are unavailable.
#    Copyright (C) 2017 Marco BODRATO
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##it
#    raffinaRegione, migliora il file di dati sulle scuole di una
#    singola regione, utilizzando le stesse coordinate quando
#    l'indirizzo coincide e le coordinate del comune quando non ci
#    sono altre informazioni.
#    Scritto nel 2017 da Marco BODRATO, che ne detiene i diritti d'autore
#
#    Questo programma è software libero: può essere ridistribuito o modificato
#    nei termini della licenza "GNU Affero General Public License" come
#    pubblicata dalla Free Software Foundation; o la versione 3 della
#    licenza, o (a vostra scelta) qualunque versione successiva.
#
#    Questo programma è distribuito con l'auspicio che sia utile,
#    ma SENZA ALCUNA GARANZIA; senza neppure la garanzia implicita di
#    VENDIBILITÀ o ADEGUATEZZA AD UN PARTICOLARE SCOPO. Si veda la
#    "GNU Affero General Public License" per ulteriori dettagli.
#
#    Dovreste aver ricevuto una copia della "GNU Affero General Public License"
#    con a questo programma. Vedere altrimenti <http://www.gnu.org/licenses/>.
#

import csv
from sys import exit
from operator import itemgetter

def leggiInfoRegioni (Regioni = {}, nomeFileDati = 'Regioni.csv', separatoreDati = '|'):
    try:
        fileCsv = open (nomeFileDati)
        csvRegioni = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvRegioni:
            Regioni[riga['DenominazioneMIUR']] = [riga['NomeFile'], riga['CR'], set()]
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()
    return Regioni

def leggiCoorComuni (Comuni = {}, nomeFileDati = 'Comuni_daLocISTAT2011.csv', separatoreDati = ','):
    try:
        fileCsv = open (nomeFileDati)
        csvComuni = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvComuni:
            Comuni[riga['CODICECOMUNE']] = (riga['Lat'], riga['Lon'], riga['CHK'])
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()
    return Comuni

def raffinaRegione (regione, datiRegione, Scuole, Comuni):
    """Riempie le coordinate mancanti usando altri punti col medesimo
    indirizzo tra quelli registrati, in seconda istanza tra quelli
    dell'anno precedente, come ultima spiaggia le coordinate ISTAT
    della località.

    """
    indirizzi = {}
    comuniCambiati = {Comuni[x][2] for x in Comuni if Comuni[x][2] != ''}
    for x in Scuole:
        ind = Scuole[x][0]
        if Scuole[x][4] in comuniCambiati and Scuole[x][8] == '?C':
            Scuole[x][8] = ''
        if Scuole[x][8][:9] == 'E3C: ' + Comuni[Scuole[x][4]][2] :
            Scuole[x][8] = ''
        if ',' in ind and ind[-4:] != " SNC" and Scuole[x][8] in ['','OSM']:
            lat = Scuole[x][6]
            lon = Scuole[x][7]
            if len (lat) + len (lon) > 2:
                chiave = Scuole[x][2] + ':' + ind
                if chiave not in indirizzi:
                    indirizzi [chiave] = (lat, lon)
    print ('Analisi zona: ', regione)
    print ('Numero indirizzi in zona: ', len (indirizzi))
    nomeFileDati = 'coordinateNonUsate.csv'
    separatoreDati = ';'
    try:
        fileCsv = open (nomeFileDati)
        csvScuole = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvScuole:
            if riga['REGIONE'][:5] == regione[:5]:
                chiave = riga['COMUNE'] + ':' + riga['INDIRIZZO']
                if chiave not in indirizzi:
                    indirizzi [chiave] = (riga['LAT'], '+' + riga['LON'])
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()
    print ('Numero indirizzi compresi non usati: ', len (indirizzi))
    for x in Scuole:
        if 'E0N' in Scuole[x][8] or 'E1R' in Scuole[x][8] or 'E2P' in Scuole[x][8] or 'E3C' in Scuole[x][8]:
            lat = ''
            lon = ''
        else:
            lat = Scuole[x][6]
            lon = Scuole[x][7]
        # if Scuole[x][8] not in ['','OSM', 'MIUR']:
        #     lat = ''
        #     lon = ''
        if len (lat) + len (lon) < 3:
            ind = Scuole[x][0]
            chiave = Scuole[x][2] + ':' + ind
            if chiave in indirizzi:
                Scuole[x][6] = indirizzi[chiave][0]
                Scuole[x][7] = indirizzi[chiave][1]
                Scuole[x][8] = 'MIUR'
            elif Scuole[x][4] in Comuni:
                Scuole[x][6] = Comuni[Scuole[x][4]][0]
                Scuole[x][7] = Comuni[Scuole[x][4]][1]
                Scuole[x][8] = 'ISTAT2011'
            else:
                print ("???")
    return

def leggiFileRegione (regione, datiRegione, Scuole, separatoreDati = ';'):
    nomeFileDati = datiRegione[0] + '.csv'
    try:
        fileCsv = open (nomeFileDati)
        csvScuole = csv.DictReader(fileCsv, delimiter = separatoreDati)
        for riga in csvScuole:
            scuola = riga['CODICESCUOLA']
            datiRegione[2].add (scuola)
            indScuola = riga['INDIRIZZO']
            chk = riga['CHK']
            latScuola = riga['LAT']
            lonScuola = riga['LON'] 
            comuneScuola = riga['COMUNE']
            Scuole[scuola] = [indScuola, riga['CAP'], comuneScuola, riga['PROVINCIA'], riga['CODICECOMUNE'], riga['ISTITUTORIFERIMENTO'], latScuola, lonScuola, chk]
    except:
        print ('Errore durante la lettura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()

def scriviFileRegione (regione, datiRegione, Scuole, separatoreDati = ';'):
    intestazione = ['CR', 'CODICESCUOLA', 'INDIRIZZO', 'CAP', 'COMUNE', 'PROVINCIA', 'CODICECOMUNE', 'ISTITUTORIFERIMENTO', 'LAT', 'LON', 'CHK']
    listaScuole = [[datiRegione[1], x] + list(Scuole[x]) for x in datiRegione[2]]
    listaScuole.sort ()
    listaScuole.sort (key = itemgetter(intestazione.index('PROVINCIA'), intestazione.index('COMUNE'), intestazione.index('ISTITUTORIFERIMENTO')))
    scriviFileCSV (datiRegione[0] + '.csv', intestazione, listaScuole, separatoreDati)

def scriviFileCSV (nomeFileDati, intestazione, righe, separatoreDati = ';'):
    try:
        fileCsv = open (nomeFileDati, 'w')
        csvScuole = csv.writer(fileCsv, delimiter = separatoreDati, quoting=csv.QUOTE_MINIMAL)
        csvScuole.writerow(intestazione)
        for riga in righe:
            csvScuole.writerow(riga)
    except:
        print ('Errore durante la scrittura di ', nomeFileDati)
        exit (1)
    finally:
        fileCsv.close ()

Regioni = leggiInfoRegioni ()
Comuni = leggiCoorComuni ()

for regione in Regioni:
    Scuole = {}
    leggiFileRegione (regione, Regioni[regione], Scuole)
    raffinaRegione (regione, Regioni[regione], Scuole, Comuni)
    scriviFileRegione (regione, Regioni[regione], Scuole)
    Regioni[regione][2] = set()
