# Resoconto operazioni effettuate #

Questo documento contiene il resoconto delle operazioni effettuate per generare questa [collezione di dati geografici sulle scuole](README.md), per eventuale replicabilità.

I dati che vengono scaricati dalla rete non verranno riprodotti in questo *repository*, verranno solo proposti qui gli indirizzi dei file e i risultati delle necessarie elaborazioni.

## 2017-07-26 ##

Purtroppo tra i dati ISTAT le "[località puntuali italiane](http://www.istat.it/storage/cartografia/basi_territoriali/localita-puntuali-italiane-shp/Localita_2011_Point_WGS84.zip)" sono ferme al 2011. Per attualizzarle si è effettuato uno *spatial-join* con gli stessi "confini amministrativi" ISTAT al 31 dicembre 2016 usati per i dati scolastici. In questo modo i diversi toponimi sono stati riassegnati secondo i confini attuali.

Per ogni comune è stato scelto un punto rappresentate, privilegiando tra i punti quello con denominazione che coincidesse o assomigliasse al nome del comune, in maniera quasi totalmente automatizzata, ma che non ha grossa rilevanza.

Il più delle volte si è selezionata l'area e quindi il punto in base al codice del comune, ma in alcuni casi i codici non corrispondevano, come ad esempio `D506-Farra d'Alpago`, `G638-Pieve d'Alpago` e `H092-Puos d'Alpago`, che i dati del MIUR danno ancora come comuni distinti, ma per i dati ISTAT sono unificati nel comune `M375-Alpago` (discrepanza che ha anche fatto sì che tutte le scuole in quei tre comuni risultassero segnate tra le discrepanze a seguito del *join*). In questi casi, nella colonna CHK è stato segnato il codice del comune nel quale stanno le coordinate scelte.

## 2017-07-15 ##

Migliorato lo script Python per effettuare una massiva (ma in certo senso *brutale*) normalizzazione degli indirizzi. Ulteriori miglioramenti sono possibili, ma il vero lavoro da fare è *manuale*, servirebbe verificare gli indirizzi in maniera puntuale.

Effettuato uno *spatial-join* tra i dati attualmente pubblicati e i "confini amministrativi" ISTAT al 31 dicembre 2016 dal file `Limiti_2016_WGS84.zip`, in modo da identificare il comune nel quale sono localizzate le coordinate.

Per poter confrontare correttamente i comuni, per i dati ISTAT è stato recuperato il *Codice Catastale del comune* dal file `Archivio-elenco-comuni-codici-e-denominazioni_Anni-2014_2017/Elenco-codici-statistici-e-denominazioni-al-31_12_2016.xls`.

Sono emerse dieci coordinate fuori dalla nazione, 87 nella regione sbagliata, 136 in un'altra provincia, 2313 in un comune non corrispondente, elencate in un file *ad hoc* [DiscrepanzeJoinISTAT.csv](DiscrepanzeJoinISTAT.csv) ed agganciate ai diversi file CSV generati aggiungendo la funzionalità al solito script Python. Sono emersi anche errori nella denominazione delle province, segnalati in #4.

## 2017-07-12 ##

Scritto un primo programma in Python: [MIUR_csv2regioni.py](MIUR_csv2regioni.py), per fronteggiare il [primo passo richiesto](#1).

Lanciandolo, sono stati generati i primi file, uno per regione, con l'elenco dei codici meccanografici, gli indirizzi parzialmente normalizzati e le coordinate che erano state pubblicate sul portale *Scuola In Chiaro*.

Delle oltre 65mila scuole catalogate, quasi 9mila non hanno nessuna indicazione di coordinate e più di 3mila, pur avendo conservato il codice meccanografico hanno cambiato il comune o l'indirizzo, quindi necessiterebbero di un controllo.

Perché le operazioni si svolgessero senza problemi, è stato necessario innanzitutto convertire al formato UTF-8 tutti i file CSV scaricati, e in particolare il file 8-Anagrafe_Scuole_Paritarie_201516.csv, che altrimenti non sarebbe stato letto dallo script.

## 2017-07-11 ##

Scaricati i seguenti file CSV dall'area [Scuole](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/?area=Scuole) del [Portale Unico dei Dati della Scuola](http://dati.istruzione.it/opendata/), contenenti a informazioni anagrafiche delle suole scuole attive nell'anno scolastico 2016/17:

  * [SCUANAGRAFESTAT20170221.csv](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/SCUANAGRAFESTAT20170221.csv);
  * [SCUANAGRAFEPAR20170221.csv](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/SCUANAGRAFEPAR20170221.csv);
  * [SCUANAAUTSTAT20170221.csv](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/SCUANAAUTSTAT20170221.csv);
  * [SCUANAAUTPAR20170221.csv](http://dati.istruzione.it/opendata/opendata/catalogo/elements1/SCUANAAUTPAR20170221.csv).

Scaricati inoltre i seguenti file CSV da quella che era l'area dati aperti del portale *Scuola In Chiaro*, contenenti, oltre all'anagrafica, anche le coordinate geografiche WGS84 di molte scuole, ma relative all'anno scolastico 2015/2016:

  * [7-Anagrafe_Scuole_Statali_201516.csv](http://www.istruzione.it/scuolainchiaro_dati/7-Anagrafe_Scuole_Statali_201516.csv);
  * [8-Anagrafe_Scuole_Paritarie_201516.csv](http://www.istruzione.it/scuolainchiaro_dati/8-Anagrafe_Scuole_Paritarie_201516.csv).

Scaricati da ISTAT:

  * dalla sezione [confini amministrativi](http://www.istat.it/it/archivio/124086), in formato shapefile, i [dati al 31 dicembre 2016](http://www.istat.it/storage/cartografia/confini_amministrativi/non_generalizzati/2016/Limiti_2016_WGS84.zip);
  * dalla sezione [codici statistici delle unità amministrative](http://www.istat.it/it/archivio/6789), l'elenco codici statistici e denominazioni delle unità territoriali e [Archivio elenco comuni, codici e denominazioni - anni 2014-2017](http://www.istat.it/storage/codici-unita-amministrative/Archivio-elenco-comuni-codici-e-denominazioni_Anni-2014_2017.zip);
  * dalla sezione [basi territoriali](http://www.istat.it/it/archivio/104317), in formato shapefile, le [località puntuali italiane](http://www.istat.it/storage/cartografia/basi_territoriali/localita-puntuali-italiane-shp/Localita_2011_Point_WGS84.zip).
